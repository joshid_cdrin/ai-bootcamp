# AI Bootcamp

AI bootcamp. A serie of machine learning and deep learning tutorials.

# How to run the notebooks ?
- Run  ```conda create  -n ai_tuto python=3.7 nb_conda ``` to create the conda environment.  
- Run ```conda activate ai_tuto``` to activate the environment.
- Run ```pip install -r requirements.txt``` to install the packages.
- Run ``` jupyter notebook``` to open the notebook. 

**Note:**
- The current directory should be ```ai-bootcamp/```
